FROM php:7.2-alpine

RUN apk update && apk add --no-cache bash git \ 
    && php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
    && php composer-setup.php \
    && php -r "unlink('composer-setup.php');" \
    # && php composer.phar global config repositories.0 '{"type":"vcs", "url":"https://github.com/razvanphp/yii2-coding-standards/"}' \
    && php composer.phar global require --prefer-dist yiisoft/yii2-coding-standards \
    && /root/.composer/vendor/bin/phpcs --config-set default_standard /root/.composer/vendor/yiisoft/yii2-coding-standards/Yii2

ENV PATH "$PATH:/root/.composer/vendor/bin"

WORKDIR /app

CMD ["phpcs"]